
# Provisioning of 3 VMs - DevOps - Final Project - G3 - Maven

$scriptDB = <<-'SCRIPT'
  echo "-------------------- updating package lists"
  sudo apt-get update -y
  echo "-------------------- installing PostgreSQL"
  # Install PostgreSQL from the official repository
  # Import the GPG repository key
  sudo apt-get install wget ca-certificates -y
  wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
  # Add the PostgreSQL repository
  sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'
  # Update the package list to ensure the latest PostgreSQL package is being installed
  sudo apt update -y
  # Install PostgreSQL and the PostgreSQL contrib package which provides additional features
  sudo apt -y install postgresql postgresql-contrib
  echo "-------------------- fixing permissions on pg_hba.conf and postgresql.conf"
  sudo sed -i "s/#listen_address.*/listen_addresses = '*'/" /etc/postgresql/12/main/postgresql.conf
  sudo sed -i '/^# IPv4 local connections.*/a host    all             all             0.0.0.0/0               trust' /etc/postgresql/12/main/pg_hba.conf
  echo "-------------------- creating PostgreSQL user with full permissions"
  sudo su postgres -c "psql -c \"CREATE USER user_devops WITH PASSWORD 'supers3cret'\" "
  echo "-------------------- creating PostgreSQL database testdb"
  sudo su postgres -c "createdb -E UTF8 -T template0 --locale=C.UTF-8 -O user_devops testdb"
  echo "-------------------- upgrading packages to latest version"
  sudo apt-get upgrade -y
SCRIPT


$scriptWEB = <<-'SCRIPT'
  echo "-------------------- updating package lists"
  sudo apt-get update -y
  echo "-------------------- installing JDK"
  sudo apt-get install openjdk-8-jdk-headless -y
  echo "-------------------- installing Maven"
  sudo apt install maven -y
  echo "-------------------- installing Nodejs and NPM via Nodejs Version Manager"
  sudo apt-get update -y
  curl -sL https://deb.nodesource.com/setup_14.x | sudo -E bash -
  sudo apt-get install -y nodejs
  echo "-------------------- uninstalling Tomcat 8"
  sudo apt-get purge --auto-remove tomcat8
  echo "-------------------- cloning the project from repository"
  git clone https://1050757@bitbucket.org/1050757/devops_h_j_j_f_e_switch2019_g3.git
  cd devops-mvn-pgsql
  echo "-------------------- assembling the application"
  sudo mvn install -DskipTests
  echo "-------------------- preparing for run frontend and backend concurrently"
  sudo chown -R $USER /home/vagrant/devops-mvn-pgsql/
  npm install
SCRIPT


Vagrant.configure('2') do |config|
  config.vm.box = 'ubuntu/bionic64'

  config.vm.provision 'shell', inline: <<-SHELL
  sudo apt-get update -y
  sudo apt-get install git -y
  sudo apt-get install iputils-ping -y
  sudo apt-get install python3 -y
  sudo apt-get install nano -y
  SHELL


  # Configurations for the database VM
  config.vm.define 'g3db' do |g3db|
    g3db.vm.box = 'ubuntu/bionic64'
    g3db.vm.hostname = 'g3db'
    g3db.vm.network 'private_network', ip: '192.168.123.12'
     # We set more ram memmory for this VM
        g3db.vm.provider 'virtualbox' do |v|
          v.memory = 2048
        end

    # To access the DB console from the host using port 8082
    g3db.vm.network 'forwarded_port', guest: 8080, host: 8082
    # To connect to the DB server using port 5432
    g3db.vm.network 'forwarded_port', guest: 5432, host: 5432

    g3db.vm.provision 'shell', inline: $scriptDB
  end


  # Configurations for the web VM
  config.vm.define 'g3web' do |g3web|
    g3web.vm.box = 'ubuntu/bionic64'
    g3web.vm.hostname = 'g3web'
    g3web.vm.network 'private_network', ip: '192.168.123.11'
    # We set more ram memmory for this VM
    g3web.vm.provider 'virtualbox' do |v|
      v.memory = 2048
    end

    # To access tomcat from the host using port 8080
    g3web.vm.network 'forwarded_port', guest: 8080, host: 8080
    # To render UI
    g3web.vm.network 'forwarded_port', guest: 3000, host: 3000

    g3web.vm.provision 'shell', inline: $scriptWEB
  end


  # Configurations for the VM holding Ansible and Jenkins
  config.vm.define 'ansible' do |ansible|
    ansible.vm.box = 'envimation/ubuntu-xenial'
    ansible.vm.hostname = 'ansible'
    ansible.vm.network 'private_network', ip: '192.168.33.10'
    # We set more ram memmory for this VM
             ansible.vm.provider 'virtualbox' do |v|
              v.memory = 2048
            end

    ansible.vm.synced_folder '.', '/vagrant', mount_options: ['dmode=775,fmode=600']

    # For acessing Jenkins on port 8081
    ansible.vm.network 'forwarded_port', guest: 8080, host: 8081

    ansible.vm.provision 'shell', inline: <<-SHELL
    sudo apt-get remove apache2* -y
    sudo apt-get install nano -y
    sudo apt-get install -y --no-install-recommends apt-utils
    sudo apt-get install software-properties-common --yes
    sudo apt-add-repository --yes --u ppa:ansible/ansible
    sudo apt-get install ansible --yes
    sudo apt install git-all -y
    sudo apt-get install openjdk-8-jre -y
    sudo apt-get install openjdk-8-jdk -y 
    sudo apt install maven -y
    sudo apt install docker.io
    sudo chmod 666 /var/run/docker.sock
    echo 'export JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/' >> /etc/profile
    echo 'export PATH=$PATH:$JAVA_HOME/bin/' >> /etc/profile
    echo 'export M2_HOME=/usr/share/maven/' >> /etc/profile
    echo 'export MAVEN_HOME=/usr/share/maven/' >> /etc/profile
    echo 'export PATH=$M2_HOME/bin:$PATH' >> /etc/profile
    # For jenkins
    wget 'http://mirrors.jenkins.io/war-stable/latest/jenkins.war'
    SHELL
  end
end
