package switch2019.project.dtoLayer.dtos;

import org.springframework.hateoas.RepresentationModel;

import java.util.List;
import java.util.Objects;

public class GroupListDTO extends RepresentationModel<GroupListDTO> {

    private List<GroupDTO> groups;

    public GroupListDTO(List<GroupDTO> groups) {
        this.groups = groups;
    }

    public GroupListDTO() {
    }

    public List<GroupDTO> getGroups() {
        return groups;
    }

    public void setGroups(List<GroupDTO> groups) {
        this.groups = groups;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GroupListDTO)) return false;
        if (!super.equals(o)) return false;
        GroupListDTO that = (GroupListDTO) o;
        return Objects.equals(getGroups(), that.getGroups());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getGroups());
    }
}