package switch2019.project.dtoLayer.dtosAssemblers;

import switch2019.project.dtoLayer.dtos.GroupDTO;
import switch2019.project.dtoLayer.dtos.GroupListDTO;

import java.util.List;

public class GroupListDTOAssembler {

    public static GroupListDTO createDTOFromDomainObject(List<GroupDTO> listOfGroupDTO) {

        GroupListDTO groupListDTO = new GroupListDTO(listOfGroupDTO);
        return  groupListDTO;
    }
}
