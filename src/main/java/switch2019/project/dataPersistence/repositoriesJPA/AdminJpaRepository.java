package switch2019.project.dataPersistence.repositoriesJPA;


import org.springframework.data.repository.CrudRepository;
import switch2019.project.dataModelLayer.dataModel.AdminJpa;

import java.util.List;

public interface AdminJpaRepository extends CrudRepository<AdminJpa, Long> {

    AdminJpa findById(long id);
    //List<AdminJpa> findAllByGroupId( GroupId id);
    List<AdminJpa> findAll();
}
