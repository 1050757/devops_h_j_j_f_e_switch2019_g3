package switch2019.project.domainLayer.repositoriesInterfaces;

import org.springframework.stereotype.Repository;
import switch2019.project.dtoLayer.dtos.DeleteGroupTransactionDTO;
import switch2019.project.dtoLayer.dtos.DeletePersonTransactionDTO;
import switch2019.project.dtoLayer.dtos.UpdateGroupTransactionDTO;
import switch2019.project.dtoLayer.dtos.UpdatePersonTransactionDTO;
import switch2019.project.domainLayer.domainEntities.aggregates.ledger.Ledger;
import switch2019.project.domainLayer.domainEntities.vosShared.LedgerID;

import java.util.Optional;

/**
 * The interface Ledger repository.
 */
@Repository
public interface ILedgerRepository {

    //-------------------------- NOVO  -----------------//

    Ledger save(Ledger ledger );

    Optional<Ledger> findById(LedgerID id);

    boolean addAndSaveTransaction(Ledger ledger);

    boolean updatePersonTransaction(Ledger ledger, UpdatePersonTransactionDTO updatePersonTransactionDTO);

    boolean updateTransaction(Ledger ledger, UpdateGroupTransactionDTO updateGroupTransactionDTO);

    boolean deletePersonTransaction(Ledger ledger, DeletePersonTransactionDTO deletePersonTransactionDTO0);

    boolean deleteTransaction(Ledger ledger, DeleteGroupTransactionDTO deleteGroupTransactionDTO);

    boolean exists(LedgerID ledgerID);

    long count();
}
