package switch2019.project.applicationLayer.applicationServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import switch2019.project.dtoLayer.dtos.GroupTransactionsWithinPeriodDTOin;
import switch2019.project.dtoLayer.dtos.GroupTransactionsWithinPeriodDTOout;
import switch2019.project.dtoLayer.dtosAssemblers.GroupTransactionsWithinPeriodDTOoutAssembler;
import switch2019.project.domainLayer.domainEntities.aggregates.group.Group;
import switch2019.project.domainLayer.domainEntities.aggregates.ledger.Ledger;
import switch2019.project.domainLayer.domainEntities.aggregates.ledger.Transaction;
import switch2019.project.domainLayer.domainEntities.vosShared.GroupID;
import switch2019.project.domainLayer.domainEntities.vosShared.LedgerID;
import switch2019.project.domainLayer.domainEntities.vosShared.PersonID;
import switch2019.project.domainLayer.exceptions.InvalidArgumentsBusinessException;
import switch2019.project.domainLayer.exceptions.NotFoundArgumentsBusinessException;
import switch2019.project.domainLayer.repositoriesInterfaces.IGroupRepository;
import switch2019.project.domainLayer.repositoriesInterfaces.ILedgerRepository;

import java.util.ArrayList;

import java.util.List;
import java.util.Optional;

/**
 * US012 - As a member of a given group, I want to obtain the transactions of that group within a given period
 *
 * @author SWitCH 2019/2020 Group 3
 * @author Joana Correia
 * @version %I%, %G%
 */

@Service
public class US012GroupTransactionsWithinPeriodService {

    @Autowired
    private final IGroupRepository groupRepository;
    @Autowired
    private final ILedgerRepository ledgerRepository;

    //Return messages

    /**
     * The constant GROUP_DOES_NOT_EXIST.
     */
    public static final String GROUP_DOES_NOT_EXIST = "Group does not exist in the system";
    /**
     * The constant PERSON_NOT_MEMBER.
     */
    public static final String PERSON_NOT_MEMBER = "Person is not member of the group";
    /**
     * The constant NO_TRANSACTIONS_TO_REPORT.
     */
    public static final String NO_TRANSACTIONS_TO_REPORT = "Within the given period, there are no transactions to report";

    /**
     * Instantiates a new Us 012 group transactions within period service.
     *
     * @param groupRepository  the group repository
     * @param ledgerRepository the ledger repository
     */
    public US012GroupTransactionsWithinPeriodService(IGroupRepository groupRepository, ILedgerRepository ledgerRepository) {
        this.groupRepository = groupRepository;
        this.ledgerRepository = ledgerRepository;
    }

    /**
     * Gets group transactions within period.
     *
     * @param groupTransactionsWithinPeriodDTOin the group transactions within period dto in
     * @return the group transactions within period
     */
    public GroupTransactionsWithinPeriodDTOout getGroupTransactionsWithinPeriod(GroupTransactionsWithinPeriodDTOin groupTransactionsWithinPeriodDTOin) throws NotFoundArgumentsBusinessException {
        Group group;
        List<Transaction> groupTransactions;

        GroupID groupID = GroupID.createGroupID(groupTransactionsWithinPeriodDTOin.getGroupDenomination());
        Optional<Group> optGroup = groupRepository.findById(groupID);

        //if group does not exist, transactions' report will not be created
        if (!optGroup.isPresent()) {
            throw new NotFoundArgumentsBusinessException(GROUP_DOES_NOT_EXIST);

        } else {
            group = optGroup.get();

            //If person is not member of the group, transactions' report cannot be created
            //Member of the group means that person may be person in charge, or merely member
            PersonID personID = PersonID.createPersonID(groupTransactionsWithinPeriodDTOin.getPersonEmail());
            boolean isPersonGroupMember = group.isPersonAlreadyMember(personID);

            if (!isPersonGroupMember) {
                throw new InvalidArgumentsBusinessException(PERSON_NOT_MEMBER);

            } else {

                //If group exist, it will have a ledger
                LedgerID groupLedgerID = group.getLedgerID();
                Optional<Ledger> optLedger = ledgerRepository.findById(groupLedgerID);
                groupTransactions = optLedger
                        .filter(groupLedger -> !groupLedger.getRecordsBetweenTwoDates(groupTransactionsWithinPeriodDTOin.getStartDate(), groupTransactionsWithinPeriodDTOin.getEndDate()).isEmpty())
                        .map(groupLedger -> groupLedger.getRecordsBetweenTwoDates(groupTransactionsWithinPeriodDTOin.getStartDate(), groupTransactionsWithinPeriodDTOin.getEndDate()))
                        .orElseThrow(() -> new NotFoundArgumentsBusinessException(NO_TRANSACTIONS_TO_REPORT));


            }
        }
        return GroupTransactionsWithinPeriodDTOoutAssembler.getGroupTransactionsWithinPeriodDTOout((ArrayList<Transaction>) groupTransactions);
    }
}
