package switch2019.project.dtoLayer.dtos;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GroupMemberClearanceDTOTest {

    @Test
    @DisplayName("GroupMemberClearanceDTO - Test Constructor with Parameters")
    public void groupMemberClearanceDTO_ConstructorWithParametersTest() {

        // Arrange
        String mariaEmail = "maria@gmail.com";
        String mariaClearance = "Admin";

        // Act
        GroupMemberClearanceDTO groupMemberClearanceDTO = new GroupMemberClearanceDTO(mariaEmail, mariaClearance);

        // Assert

        assertEquals(mariaEmail, groupMemberClearanceDTO.getMemberID());
        assertEquals(mariaClearance, groupMemberClearanceDTO.getClearance());
    }

    @Test
    @DisplayName("GroupMemberClearanceDTO - Test Constructor without Parameters")
    public void groupMemberClearanceDTO_ConstructorWithoutParametersTest() {

        // Arrange
        String mariaEmail = "maria@gmail.com";
        String mariaClearance = "Admin";

        // Act
        GroupMemberClearanceDTO groupMemberClearanceDTO = new GroupMemberClearanceDTO();
        groupMemberClearanceDTO.setMemberID(mariaEmail);
        groupMemberClearanceDTO.setClearance(mariaClearance);

        // Assert

        assertEquals(mariaEmail, groupMemberClearanceDTO.getMemberID());
        assertEquals(mariaClearance, groupMemberClearanceDTO.getClearance());
    }

    @Test
    @DisplayName("GroupMemberClearanceDTO - Test Equals || Same Object")
    public void groupMemberClearanceDTO_EqualsTest_SameObject() {

        // Arrange
        String mariaEmail = "maria@gmail.com";
        String mariaClearance = "Admin";

        // Expected
        GroupMemberClearanceDTO expectedGroupMemberClearanceDTO = new GroupMemberClearanceDTO(mariaEmail, mariaClearance);

        // Act
        GroupMemberClearanceDTO groupMemberClearanceDTO = new GroupMemberClearanceDTO(mariaEmail, mariaClearance);

        // Assert

        assertEquals(expectedGroupMemberClearanceDTO, groupMemberClearanceDTO);
        assertEquals(groupMemberClearanceDTO, groupMemberClearanceDTO); // a object is equals to it self
    }

    @Test
    @DisplayName("GroupMemberClearanceDTO - Test Equals || Different Object")
    public void groupMemberClearanceDTO_EqualsTest_DifferentObject() {

        // Arrange
        String mariaEmail = "maria@gmail.com";
        String mariaClearance = "Admin";

        String manuelEmail = "manuel@gmail.com";
        String manuelClearance = "Member";

        String bugKiller = "Bug Killer";


        // Expected
        GroupMemberClearanceDTO expectedGroupMemberClearanceDTO = new GroupMemberClearanceDTO(mariaEmail, mariaClearance);

        // Act
        GroupMemberClearanceDTO groupMemberClearanceDTO = new GroupMemberClearanceDTO(manuelEmail, manuelClearance);

        // Assert

        assertNotEquals(expectedGroupMemberClearanceDTO, groupMemberClearanceDTO);
        assertNotEquals(bugKiller, groupMemberClearanceDTO); // not same instance
    }

    @Test
    @DisplayName("GroupMemberClearanceDTO - Test Hash Code")
    public void groupMemberClearanceDTO_HashCodeTest() {

        // Arrange
        String mariaEmail = "maria@gmail.com";
        String mariaClearance = "Admin";

        // Expected

        int expectedHashCode = -613286295;


        // Act
        GroupMemberClearanceDTO groupMemberClearanceDTO = new GroupMemberClearanceDTO(mariaEmail, mariaClearance);
        int hashcode = groupMemberClearanceDTO.hashCode();

        // Assert

        assertEquals(expectedHashCode,hashcode);
    }


}