package switch2019.project.dtoLayer.dtos;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GroupListDTOTest {

    @Test
    @DisplayName("GroupAllMembersDTO - Test Constructor with Parameters")
    void groupListDTO_ConstructorWithParametersTest() {

        //Arrange
        String groupDenomination = "Sunday Runners";
        String groupDescription = "All members from Sunday Runners group";
        String groupDateOfCreation = LocalDate.now().toString();

        GroupDTO groupDTO = new GroupDTO(groupDenomination, groupDescription, groupDateOfCreation);

        List<GroupDTO> groups = new ArrayList<>();
        groups.add(groupDTO);

        // Act
        GroupListDTO groupListDTO = new GroupListDTO(groups);

        // Assert
        assertEquals(groups, groupListDTO.getGroups());
    }

    @Test
    @DisplayName("GroupAllMembersDTO - Test Constructor without Parameters")
    void groupListDTO_ConstructorWithoutParametersTest() {

        //Arrange
        String groupDenomination = "Sunday Runners";
        String groupDescription = "All members from Sunday Runners group";
        String groupDateOfCreation = LocalDate.now().toString();

        GroupDTO groupDTO = new GroupDTO(groupDenomination, groupDescription, groupDateOfCreation);

        List<GroupDTO> groups = new ArrayList<>();
        groups.add(groupDTO);

        // Act
        GroupListDTO groupListDTO = new GroupListDTO();
        groupListDTO.setGroups(groups);

        // Assert
        assertEquals(groups, groupListDTO.getGroups());
    }

    @Test
    @DisplayName("GroupAllMembersDTO - Test Equals || Same Object")
    void groupListDTO_EqualsTest_SameObject() {

        // Arrange
        String groupDenomination = "Sunday Runners";
        String groupDescription = "All members from Sunday Runners group";
        String groupDateOfCreation = LocalDate.now().toString();

        GroupDTO groupDTO = new GroupDTO(groupDenomination, groupDescription, groupDateOfCreation);

        List<GroupDTO> groups = new ArrayList<>();
        groups.add(groupDTO);

        // Expected
        GroupListDTO expectedGroupListDTO = new GroupListDTO(groups);

        // Act
        GroupListDTO groupListDTO = new GroupListDTO();
        groupListDTO.setGroups(groups);

        // Assert
        assertEquals(expectedGroupListDTO, groupListDTO);
        assertEquals(groupListDTO, groupListDTO);
    }

    @Test
    @DisplayName("GroupAllMembersDTO - Test Equals || Different Object")
    void groupListDTO_EqualsTest_DifferentObject() {

        // Arrange

        //Group Sunday Runners
        String sundayRunnersDenomination = "Sunday Runners";
        String sundayRunnersDescription = "All members from Sunday Runners group";
        LocalDate sundayRunnersDateOfCreation = LocalDate.of(1973, 07, 25);

        //Group Silva Family
        String silvaFamilyDenomination = "Silva Family";
        String silvaFamilyDescription = "All members from Silva family";
        LocalDate silvaFamilyDateOfCreation = LocalDate.of(1975, 07, 25);

        GroupDTO groupDTO1 = new GroupDTO(sundayRunnersDenomination, sundayRunnersDescription, sundayRunnersDateOfCreation.toString());
        GroupDTO groupDTO2 = new GroupDTO(silvaFamilyDenomination, silvaFamilyDescription, silvaFamilyDateOfCreation.toString());

        List<GroupDTO> groups = new ArrayList<>();
        groups.add(groupDTO1);

        List<GroupDTO> groups2 = new ArrayList<>();
        groups2.add(groupDTO1);
        groups2.add(groupDTO2);

        String bugKiller = "Bug Killer";

        // Act
        GroupListDTO groupListDTO1 = new GroupListDTO(groups2);

        GroupListDTO groupListDTO2 = new GroupListDTO(groups);

        // Assert
        assertNotEquals(groupListDTO1, groupListDTO2);
        assertNotEquals(bugKiller, groupListDTO1); // not same instance
    }


    @Test
    @DisplayName("GroupAllMembersDTO - Test Hash Code")
    void groupListDTO_HashCodeTest() {

//        // Arrange
//        String groupDenomination = "Sunday Runners";
//        String groupDescription = "All members from Sunday Runners group";
//        String groupDateOfCreation = LocalDate.now().toString();
//
//        GroupDTO groupDTO = new GroupDTO(groupDenomination, groupDescription, groupDateOfCreation);
//
//        List<GroupDTO> groups = new ArrayList<>();
//        groups.add(groupDTO);
//
//        int expectedHashCode = 562853766;
//
//        // Act
//        GroupListDTO groupListDTO = new GroupListDTO(groups);
//
//        // Assert
//        assertEquals(expectedHashCode, groupListDTO.hashCode());
    }

}